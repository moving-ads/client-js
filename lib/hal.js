'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _lodash = require('lodash');

var _interpolateUrl = require('interpolate-url');

var _interpolateUrl2 = _interopRequireDefault(_interpolateUrl);

var _interceptor = require('./interceptor');

var _interceptor2 = _interopRequireDefault(_interceptor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var buildUrl = function buildUrl(object, rel, pathParams) {
  return (0, _interpolateUrl2.default)(object._links[rel || 'self'].href, pathParams);
};

var loadFrom = function loadFrom(client, object, rel, data, pathParams) {
  var url = buildUrl(object, rel, pathParams);
  return client.get(url, data);
};

var destroy = function destroy(client, object, rel, pathParams) {
  var url = buildUrl(object, rel, pathParams);
  return client.delete(url);
};

var postTo = function postTo(client, object, rel, data, pathParams) {
  var url = buildUrl(object, rel, pathParams);
  return client.post(url, data);
};

var patch = function patch(client, object, rel, data, pathParams) {
  var url = buildUrl(object, rel, pathParams);
  return client.patch(url, data);
};

var resultData = function resultData(response) {
  return Promise.resolve(response.data);
};
var rejectError = function rejectError(_ref) {
  var response = _ref.response;
  return Promise.reject(response.data);
};

var createClient = function createClient(baseUrl, getSession, staticCatalog) {
  var client = _axios2.default.create({ baseURL: baseUrl });
  client.interceptors.request.use((0, _interceptor2.default)(getSession));
  client.interceptors.response.use(resultData, rejectError);

  return {
    loadFrom: (0, _lodash.curry)(loadFrom, 2)(client),
    destroy: (0, _lodash.curry)(destroy, 2)(client),
    postTo: (0, _lodash.curry)(postTo, 2)(client),
    patch: (0, _lodash.curry)(patch, 2)(client),
    post: (0, _lodash.curry)(postTo, 3)(client, staticCatalog),
    load: (0, _lodash.curry)(loadFrom, 3)(client, staticCatalog),
    destroyAll: function destroyAll(objects) {
      return _axios2.default.all(objects.map(function (object) {
        return destroy(client, object);
      }));
    }
  };
};

exports.default = createClient;