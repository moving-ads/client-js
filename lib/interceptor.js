"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (getSession) {
  return function (config) {
    return getSession().then(function (session) {
      return session ? Object.assign({}, config, {
        headers: Object.assign({}, config.headers, {
          Authorization: "Bearer " + session.token
        })
      }) : config;
    });
  };
};