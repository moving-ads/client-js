'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});


var middleware = function middleware(failoverAction) {
  return function (store) {
    return function (next) {
      return function (action) {
        return Promise.resolve(next(action)).catch(function (error) {
          if (error.code === 'unauthorized') {
            return store.dispatch(failoverAction);
          }
          return error;
        });
      };
    };
  };
};

exports.default = middleware;