'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.open = exports.close = exports.openSocket = exports.join = exports.push = undefined;

var _phoenixSocket = require('phoenix-socket');

var _lodash = require('lodash');

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var eventLogger = function eventLogger() {
  var _console;

  for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  (_console = console).log.apply(_console, ['POLLING:'].concat(args));
};

var push = exports.push = function push(channel) {
  for (var _len2 = arguments.length, args = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
    args[_key2 - 1] = arguments[_key2];
  }

  return new Promise(function (resolve, reject) {
    channel.push.apply(channel, _toConsumableArray(args)).receive('error', reject).receive('ok', resolve);
  });
};

var join = exports.join = function join(socket, topic) {
  var args = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  return new Promise(function (resolve, reject) {
    var channel = socket.channel(topic, args);
    channel.join().receive('ok', function () {
      return resolve(channel);
    }).receive('error', reject);
  });
};

var openSocket = exports.openSocket = function openSocket(url, session) {
  return new Promise(function (resolve, reject) {
    var socket = new _phoenixSocket.Socket(url, {
      params: { token: session.token },
      logger: eventLogger
    });
    socket.onOpen(function () {
      resolve(socket);
    });
    socket.onError(function () {
      reject({ code: 'unauthorized' });
    });
    socket.connect();
  });
};

var close = exports.close = function close(socket) {
  return new Promise(function (resolve) {
    socket.disconnect();
    resolve();
  });
};

var open = exports.open = function open(baseUrl, getSession) {
  return getSession().then(function (session) {
    if (session) {
      return openSocket(baseUrl + '/socket', session);
    }
    return Promise.reject({ code: 'unauthorized' });
  });
};

var pollingChannel = function pollingChannel(socket, channel) {
  return {
    on: function on(event, callback) {
      return channel.on(event, callback);
    },
    push: (0, _lodash.partial)(push, channel),
    close: (0, _lodash.partial)(close, socket)
  };
};

var mapCallbacks = function mapCallbacks(callbacks, channel) {
  (0, _lodash.each)(callbacks, function (callback, event) {
    channel.on(event, callback);
  });
  return channel;
};

exports.default = function (baseUrl, getSession) {
  return function (topic, callbacks) {
    return open(baseUrl, getSession).then(function (socket) {
      return join(socket, topic).then((0, _lodash.partial)(mapCallbacks, callbacks)).then((0, _lodash.partial)(pollingChannel, socket));
    });
  };
};