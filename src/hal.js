/* @flow */

import axios from 'axios'
import { curry } from 'lodash'
import interpolateUrl from 'interpolate-url'

import type { GetSession } from './session'

import interceptor from './interceptor'

export type HalLink = {
  href: string,
}
export type HalLinkCollection = { [rel: string]: HalLink }
export type HalResource = {
  _links: HalLinkCollection,
}

const buildUrl = (object: HalResource, rel?: string, pathParams?: any) => (
  interpolateUrl(object._links[rel || 'self'].href, pathParams)
)

const loadFrom = (client, object: HalResource, rel?: string, data?: any, pathParams?: any) => {
  const url = buildUrl(object, rel, pathParams)
  return client.get(url, data)
}

const destroy = (client, object: HalResource, rel?: string, pathParams?: any) => {
  const url = buildUrl(object, rel, pathParams)
  return client.delete(url)
}

const postTo = (client, object: HalResource, rel?: string, data: any, pathParams: any) => {
  const url = buildUrl(object, rel, pathParams)
  return client.post(url, data)
}

const patch = (client, object: HalResource, rel?: string, data: any, pathParams: any) => {
  const url = buildUrl(object, rel, pathParams)
  return client.patch(url, data)
}

const resultData = (response: any): Promise<any> => Promise.resolve(response.data)
const rejectError = ({ response }): Promise<any> => Promise.reject(response.data)

const createClient = (baseUrl: string, getSession: GetSession, staticCatalog: Object) => {
  const client = axios.create({ baseURL: baseUrl })
  client.interceptors.request.use(interceptor(getSession))
  client.interceptors.response.use(resultData, rejectError)

  return {
    loadFrom: curry(loadFrom, 2)(client),
    destroy: curry(destroy, 2)(client),
    postTo: curry(postTo, 2)(client),
    patch: curry(patch, 2)(client),
    post: curry(postTo, 3)(client, staticCatalog),
    load: curry(loadFrom, 3)(client, staticCatalog),
    destroyAll: (objects: Array<HalResource>) => (
      axios.all(
        objects.map((object: HalResource) => destroy(client, object))
      )
    ),
  }
}

export default createClient
