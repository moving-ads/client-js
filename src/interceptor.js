/* @flow */

import type { Session, GetSession } from "./session";

export default (getSession: GetSession) => (config: Object): Promise<Object> => (
  getSession().then((session) => (
    (session) ? Object.assign({}, config, {
      headers: Object.assign({}, config.headers, {
          Authorization: `Bearer ${session.token}`,
      }),
    }): config
  ))
)
