import type { Middleware, MiddlewareAPI } from 'redux'

const middleware = (failoverAction) => (
  (store) => (next) => (action) => (
    Promise.resolve(next(action))
      .catch((error: any): any => {
        if (error.code === 'unauthorized') {
          return store.dispatch(failoverAction)
        }
        return error
      })
  )
)

export default middleware
