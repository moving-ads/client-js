// @flow

import { Socket } from 'phoenix-socket'
import type { Channel } from 'phoenix-socket'
import { each, partial } from 'lodash'

import type { GetSession, Session } from './session'

const eventLogger = (...args) => {
  console.log('POLLING:', ...args)
}

export const push = (channel: Channel, ...args: any[]): Promise<any> => {
  return new Promise((resolve, reject) => {
    channel.push(...args)
      .receive('error', reject)
      .receive('ok', resolve)
  })
}

export const join = (socket: Socket, topic: string, args: Object = {}): Promise<Channel> => {
  return new Promise((resolve, reject) => {
    const channel = socket.channel(topic, args)
    channel.join()
      .receive('ok', () => resolve(channel))
      .receive('error', reject)
  })
}

export const openSocket = (url: string, session: Session): Promise<Socket> => {
  return new Promise((resolve, reject) => {
    const socket = new Socket(url, {
      params: { token: session.token },
      logger: eventLogger,
    })
    socket.onOpen(() => {
      resolve(socket)
    })
    socket.onError(() => {
      reject({ code: 'unauthorized' })
    })
    socket.connect()
  })
}

export const close = (socket: Socket): Promise<void> => (
  new Promise((resolve) => {
    socket.disconnect()
    resolve()
  })
)

export const open = (baseUrl: string, getSession: GetSession): Promise<Socket> => (
  getSession().then((session) => {
    if (session) {
      return openSocket(`${baseUrl}/socket`, session)
    }
    return Promise.reject({ code: 'unauthorized' })
  })
)

export type MessageCallback = (any) => void

export type CallbackMap = {
  [event: string]: MessageCallback
}

export type PollingChannel = {
  on: (string, MessageCallback) => void,
  push: (...args: any[]) => Promise<any>,
  close: () => Promise<void>,
}

const pollingChannel = (socket: Socket, channel: Channel): PollingChannel => ({
  on: (event: string, callback: MessageCallback) => channel.on(event, callback),
  push: partial(push, channel),
  close: partial(close, socket),
})

const mapCallbacks = (callbacks: CallbackMap, channel: Channel): Channel => {
  each(callbacks, (callback: MessageCallback, event: string) => {
    channel.on(event, callback)
  })
  return channel
}

export default (baseUrl: string, getSession: GetSession) => (
  (topic: string, callbacks: CallbackMap): Promise<PollingChannel> => (
    open(baseUrl, getSession).then((socket: Socket): Promise<PollingChannel> => (
      join(socket, topic)
        .then(partial(mapCallbacks, callbacks))
        .then(partial(pollingChannel, socket))
    ))
  )
)
