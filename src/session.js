/* @flow */

export type Session = {|
  token: string,
  user: any,
|}

export type GetSession = () => Promise<?Session>
